package ru.lanit.hcs.cms;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

public class PushoverMessage {
    private String token;
    private String user;
    private String message;
    private String title;
    private  int priority;

    public PushoverMessage(String token, String user, String message, String title, int priority) {
        this.token = token;
        this.user = user;
        this.message = message;
        this.title = title;
        this.priority = priority;
    }

    public MultiValueMap<String, String> getAsMap(){
        MultiValueMap<String, String> result = new LinkedMultiValueMap<>();
        result.add("token", this.token);
        result.add("user", this.user);
        result.add("message", this.message);
        result.add("title", this.title);
        result.add("priority", this.priority+"");
        return  result;
    }

    public String getToken() {
        return token;
    }

    public String getUser() {
        return user;
    }

    public String getMessage() {
        return message;
    }

    public String getTitle() {
        return title;
    }

    public int getPriority() {
        return priority;
    }
}
