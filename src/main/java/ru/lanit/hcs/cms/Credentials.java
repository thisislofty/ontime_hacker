package ru.lanit.hcs.cms;

public class Credentials {

    private String phoneNumber;
    private String password;
    private boolean useRefreshTokens = true;
    private String userKind = "CateringClient";

    public Credentials (String phoneNumber, String password){
        this.phoneNumber = phoneNumber;
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isUseRefreshTokens() {
        return useRefreshTokens;
    }

    public void setUseRefreshTokens(boolean useRefreshTokens) {
        this.useRefreshTokens = useRefreshTokens;
    }

    public String getUserKind() {
        return userKind;
    }

    public void setUserKind(String userKind) {
        this.userKind = userKind;
    }
}
