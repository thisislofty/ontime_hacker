package ru.lanit.hcs.cms;

import java.time.LocalDateTime;

public class NotificationService {

    private String apiKey; //"aci265hxkqdimfkcvpkavprh6bh4n9"
    private String userKey; //"uesqn8pbn31uwx3c81dgi9rigudc1c"
    private final String EXCHANGE_URL = "https://api.pushover.net/1/messages.json";

    public NotificationService(String apiKey, String userKey) {
        this.apiKey = apiKey;
        this.userKey = userKey;
    }

    public boolean sendMessage(String title, String text, int priority) {
        System.out.println(LocalDateTime.now() + " | Отправка сообщения " + title + " / " + text );
        PushoverMessage message = new PushoverMessage(apiKey, userKey, text, title, priority);
        try {
            Utils.httpPost(EXCHANGE_URL, message.getAsMap());
        } catch (Exception e) {
            System.err.println("WARNING!! CAN'T SEND NOTIFICATION!!! ОСТАНЕШЬСЯ ГОЛОДНЫМ ТЕПЕРЬ БЛЯДЬ " + e.getMessage());
            return false;
        }
        return true;
    }

}
