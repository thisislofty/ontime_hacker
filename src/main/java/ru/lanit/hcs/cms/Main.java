package ru.lanit.hcs.cms;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main (String [] args){
        OntimeApi api = new OntimeApi(args[1], args[0]);
        NotificationService notificationService = new NotificationService("aci265hxkqdimfkcvpkavprh6bh4n9", "uesqn8pbn31uwx3c81dgi9rigudc1c");
        notificationService.sendMessage("Starting applications", "Application for free food was started", -1);
        System.out.println("===APPLICATION FOR FREE FOOD===");
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(2);
        executor.scheduleAtFixedRate(()->{
            System.out.println(LocalDateTime.now() + " | Запускается экзекьютор");
            if((api.checkHypeActionInCustomersInfo() || api.checkHypeActionByShortUrls()) && (LocalDateTime.now().getHour()>9 && LocalDateTime.now().getHour()>11)){
                notificationService.sendMessage("ЖРААТЬ", "Кажется, халявная жратва, нука проверь быстренько, щегол",1);
                Smsc sms = new Smsc();
                Arrays.stream(args).skip(1).forEach(number -> {
                    sms.send_sms(number,"ЖРАААТЬ", 0, "","",0, "FREE FOOD", "");
                });
            }
        },0, 1, TimeUnit.MINUTES);

        try {
            executor.awaitTermination(4, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        executor.shutdown();

    }
}
