package ru.lanit.hcs.cms;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class OntimeApi {

    private String userid;

    public OntimeApi(String phoneNumber, String password) {
        this.userid = login(phoneNumber, password);
    }

    private String login(String phoneNumber, String password) {
        Gson gson = new Gson();
        Optional<JsonElement> json = Optional.empty();
        try {
            json = Optional.ofNullable(gson.fromJson(Utils.httpPost("https://api-ot-prod.logistictech.ru/client.service/api/user/login", gson.toJson(new Credentials(phoneNumber, password))), JsonElement.class));
        } catch (Exception e) {
            System.err.println("Can't login " + e.getMessage());
        }
        json.ifPresent(data -> System.out.println("Login successful"));
        return json.get().getAsJsonObject().get("id").getAsString();
    }

    private List<String> getShortUrls() {
        Gson gson = new Gson();
        List<String> result = new ArrayList<>();
        Optional<JsonElement> json = Optional.empty();
        try {
            json = Optional.ofNullable(gson.fromJson(Utils.httpGet("https://ontime.online/api/link/" + this.userid + "/"), JsonElement.class));
        } catch (Exception e) {
            e.printStackTrace();
        }

        json.ifPresent(jsonElement -> {
            jsonElement.getAsJsonObject().getAsJsonArray("soffer").forEach(item -> result.add(item.getAsJsonObject().get("short_url").getAsString()));
        });
        System.out.println("Get shorted urls " + result.size());
        return result;

    }

    public boolean checkHypeActionInCustomersInfo() {
        Gson gson = new Gson();
        Optional<JsonElement> json = Optional.empty();
        try {
            json = Optional.ofNullable(gson.fromJson(Utils.httpGet("https://ontime.online/api/customers2/" + this.userid + "/"), JsonElement.class));
        } catch (Exception e) {
            System.err.println("Cant get HypeActions from customer info" + e.getMessage());
        }
        JsonObject o1 = json.get().getAsJsonObject();
        JsonObject o2 = o1.getAsJsonObject("groupclient");
        JsonElement o4 = o2.get("tga");
        boolean res = o4.isJsonNull();
        boolean result = json.isPresent() && !res;
        System.out.println(LocalDateTime.now() + "| Проверка по customersInfo = " + result);
        return result;
    }

    public boolean checkHypeActionByShortUrls() {
        Gson gson = new Gson();
        List<String> shortUrls = getShortUrls();
        return shortUrls.stream().anyMatch(shortUrl -> {
            Optional<JsonElement> json = Optional.empty();
            try {
                json = Optional.ofNullable(gson.fromJson(Utils.httpGet("https://ontime.online/api/" + shortUrl + "/usermenu"), JsonElement.class));
            } catch (Exception e) {
                System.err.println("Cant get HypeAction by shortUrl " + shortUrl + " :" + e.getMessage());
            }
            boolean result = json.isPresent() && !json.get().getAsJsonObject().getAsJsonObject("user").getAsJsonObject("user").getAsJsonObject("groupclient").get("tga").isJsonNull();
            System.out.println(LocalDateTime.now() + "| Проверка по short_url " + shortUrl + " = " + result);
            return result;
        });

    }
}
